# Summary

* [Introduction](README.md)
* Raw
    - [5/Facilities](5.md)
    - [6/Admissions](6.md)
    - [7/Autosave](7.md)
    - [8/ST](8.md)
* Draft
    - [3/Types](3.md)
    - [4/Groups](4.md)
* Stable
    - [1/GCG](1.md)
    - [2/Spec](2.md)
